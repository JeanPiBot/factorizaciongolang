package main

import "fmt"


func main() {
	number := 24
	factores := []int{}
	middle := number / 2

	for i := 1; i <= middle; i++ {
		if number % i == 0 {
			factores = append(factores, i)
		}
	}

	fmt.Println("Los factores de ", number," son: ",factores)
}